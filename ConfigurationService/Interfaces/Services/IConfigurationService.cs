﻿using Tilde.EMW.Contracts.Models.Services.Configuration.Configuration;

namespace Tilde.ConfigurationService.Interfaces.Services
{
	public interface IConfigurationService
	{
		/// <summary>
		/// List all available configurations
		/// </summary>
		/// <returns></returns>
		public Task<Models.DTO.Configuration.ConfigurationList> List();

		/// <summary>
		/// Create new configuration
		/// </summary>
		/// <param name="config"> New configuration model </param>
		/// <returns></returns>
		public Task<Configuration> Create(NewConfiguration config);

		/// <summary>
		/// Get configuration by its unique identifier
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <returns></returns>
		public Task<Configuration> Get(Guid id);

		/// <summary>
		/// Update configuration
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <param name="config"> Configuration update model </param>
		/// <returns></returns>
		public Task<Configuration> Update(Guid id, Models.DTO.Configuration.ConfigurationUpdate config);

		/// <summary>
		/// Delete configuration by its unique identifier
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <returns></returns>
		public Task<Configuration> Delete(Guid id);
	}
}
