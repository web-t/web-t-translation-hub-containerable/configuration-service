﻿using Tilde.EMW.Contracts.Models.Services.Configuration.Provider;

namespace Tilde.ConfigurationService.Interfaces.Services
{
    public interface IProviderService
    {
        /// <summary>
        /// Get provider
        /// </summary>
        /// <returns></returns>
        public Task<Provider> Get();

        /// <summary>
        /// Create a new provider
        /// </summary>
        /// <param name="provider"> New provider model </param>
        /// <returns></returns>
        public Task<Provider> Create(Models.DTO.Provider.NewProvider provider);
    }
}
