﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Tilde.ConfigurationService.Interfaces.Services;
using Tilde.ConfigurationService.Models.Configuration;
using Tilde.ConfigurationService.Models.Database;
using Database = Tilde.ConfigurationService.Models.Database;
using ExtrenalDTO = Tilde.EMW.Contracts.Models.Services.Configuration.Provider;
using InternalDTO = Tilde.ConfigurationService.Models.DTO.Provider;

namespace Tilde.ConfigurationService.Services
{
    public class ProviderService : IProviderService
    {
        private readonly ILogger _logger;
        private readonly ServiceDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ConfigurationServices _configurationServices;

        public ProviderService(
            ILogger<ProviderService> logger,
            ServiceDbContext dbContext,
            IMapper mapper,
            IOptions<ConfigurationServices> configurationServices
        )
        {
            _logger = logger;
            _dbContext = dbContext;
            _mapper = mapper;
            _configurationServices = configurationServices.Value;
        }

        /// <inheritdoc/>
        public async Task<ExtrenalDTO.Provider> Get()
        {
            var provider = await _dbContext.Providers
                .FirstOrDefaultAsync();

            if (provider == null)
            {
                return new ExtrenalDTO.Provider();
            }

            return _mapper.Map<ExtrenalDTO.Provider>(provider);
        }

        /// <inheritdoc/>
        public async Task<ExtrenalDTO.Provider> Create(InternalDTO.NewProvider provider)
        {
            var url = provider.Type == EMW.Contracts.Enums.Provider.Type.ETranslation ? _configurationServices.ETranslationProvider.Uri : provider.Url;
            var newProvider = _mapper.Map<Database.Provider>(provider, opt =>
                opt.AfterMap((src, dest) => dest.Url = url)
            );
            newProvider.Id = Guid.NewGuid();

            var addedProviders = await _dbContext.Providers
                .ToListAsync();

            _dbContext.Providers.RemoveRange(addedProviders);

            _dbContext.Providers.Add(newProvider);

            await _dbContext.SaveChangesAsync();

            return _mapper.Map<ExtrenalDTO.Provider>(newProvider);
        }
    }
}
