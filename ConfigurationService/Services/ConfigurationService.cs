﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Tilde.ConfigurationService.Interfaces.Services;
using Tilde.ConfigurationService.Models.Database;
using Tilde.EMW.Contracts.Exceptions.Configuration;
using Database = Tilde.ConfigurationService.Models.Database;
using ExtrenalDTO = Tilde.EMW.Contracts.Models.Services.Configuration.Configuration;
using InternalDTO = Tilde.ConfigurationService.Models.DTO.Configuration;

namespace Tilde.ConfigurationService.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly ServiceDbContext _dbContext;
        private readonly IMapper _mapper;

        public ConfigurationService(
            ServiceDbContext dbContext,
            IMapper mapper
        )
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task<ExtrenalDTO.Configuration> Create(ExtrenalDTO.NewConfiguration config)
        {
            var newConfig = _mapper.Map<Database.Configuration>(config);
            newConfig.Id = Guid.NewGuid();

            _dbContext.Configurations.Add(newConfig);

            await _dbContext.SaveChangesAsync();

            return _mapper.Map<ExtrenalDTO.Configuration>(newConfig);

        }

        /// <inheritdoc/>
        public async Task<ExtrenalDTO.Configuration> Delete(Guid id)
        {
            var config = await _dbContext.Configurations
                .Where(item => item.Id == id)
                .Include(item => item.Languages)
                .Include(item => item.SiteUrls)
                .FirstOrDefaultAsync();

            if (config == null)
            {
                throw new ConfigurationNotFoundException(id);
            }

            _dbContext.Configurations.Remove(config);

            await _dbContext.SaveChangesAsync();

            return _mapper.Map<ExtrenalDTO.Configuration>(config);
        }

        /// <inheritdoc/>
        public async Task<ExtrenalDTO.Configuration> Get(Guid id)
        {
            var config = await _dbContext.Configurations
                .Where(item => item.Id == id)
                .Include(item => item.Languages)
                .Include(item => item.SiteUrls)
                .FirstOrDefaultAsync();

            if (config == null)
            {
                throw new ConfigurationNotFoundException(id);
            }

            return _mapper.Map<ExtrenalDTO.Configuration>(config);
        }

        /// <inheritdoc/>
        public async Task<InternalDTO.ConfigurationList> List()
        {
            var results = await _dbContext.Configurations
                .Include(item => item.Languages)
                .Include(item => item.SiteUrls)
                .OrderByDescending(item => item.CreatedAt)
                .ToListAsync();

            return new InternalDTO.ConfigurationList()
            {
                Configurations = results.Select(item => _mapper.Map<ExtrenalDTO.Configuration>(item))
            };
        }

        /// <inheritdoc/>
        public async Task<ExtrenalDTO.Configuration> Update(Guid id, InternalDTO.ConfigurationUpdate configUpdate)
        {
            var config = await _dbContext.Configurations
                .Where(item => item.Id == id)
                .Include(item => item.Languages)
                .Include(item => item.SiteUrls)
                .FirstOrDefaultAsync();

            if (config == null)
            {
                throw new ConfigurationNotFoundException(id);
            }

            _mapper.Map(configUpdate, config);

            _dbContext.Configurations.Update(config);

            await _dbContext.SaveChangesAsync();

            return _mapper.Map<ExtrenalDTO.Configuration>(config);
        }
    }
}
