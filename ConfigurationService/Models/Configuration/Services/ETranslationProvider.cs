﻿namespace Tilde.ConfigurationService.Models.Configuration.Services
{
    public class ETranslationProvider
    {
        /// <summary>
        /// ETranslation provider URI
        /// </summary>
        public string Uri { get; init; }
    }
}
