﻿namespace Tilde.ConfigurationService.Models.Configuration
{
    public class ConfigurationServices
    {
        public Services.Database Database { get; init; }
        public Services.ETranslationProvider ETranslationProvider { get; init; }
    }
}
