﻿using AutoMapper;
using ExternalDTO = Tilde.EMW.Contracts.Models.Services.Configuration;

namespace Tilde.ConfigurationService.Models.Mappings
{
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Custom mapping functions for mapping data between database and data transfer object models
        /// </summary>
        public MappingProfile()
        {
            CreateMap<ExternalDTO.Configuration.NewConfiguration, Database.Configuration>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.SourceLanguage, opt => opt.MapFrom(src => src.SourceLanguage))
                .ForMember(dest => dest.Languages, opt => opt.MapFrom(src => src.Languages))
                .ForMember(dest => dest.SiteUrls, opt => opt.MapFrom(src => src.SiteUrls))
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => DateTime.Now))
                .ReverseMap();

            CreateMap<Database.Configuration, ExternalDTO.Configuration.Configuration>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.SourceLanguage, opt => opt.MapFrom(src => src.SourceLanguage))
                .ForMember(dest => dest.Languages, opt => opt.MapFrom(src => src.Languages))
                .ForMember(dest => dest.SiteUrls, opt => opt.MapFrom(src => src.SiteUrls.Where(x => x != null).Select(x => x.Url)));

            CreateMap<DTO.Configuration.ConfigurationUpdate, Database.Configuration>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.SourceLanguage, opt => opt.MapFrom(src => src.SourceLanguage))
                .ForMember(dest => dest.Languages, opt => opt.MapFrom(src => src.Languages))
                .ForMember(dest => dest.SiteUrls, opt => opt.MapFrom(src => src.SiteUrls))
                .ReverseMap();

            CreateMap<DTO.Provider.NewProvider, Database.Provider>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Authentication.BasicAuthentication.Username))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Authentication.BasicAuthentication.Password))
                .ForMember(dest => dest.ApiKey, opt => opt.MapFrom(src => src.Authentication.ApiKey))
                .ReverseMap();

            CreateMap<Database.Provider, ExternalDTO.Provider.Provider>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => src.Url))
                .ForPath(dest => dest.Authentication.BasicAuthentication.Username, opt => opt.MapFrom(src => src.Username))
                .ForPath(dest => dest.Authentication.BasicAuthentication.Password, opt => opt.MapFrom(src => src.Password))
                .ForPath(dest => dest.Authentication.ApiKey, opt => opt.MapFrom(src => src.ApiKey));

            CreateMap<ExternalDTO.Language.Language, Database.Language>()
                .ForMember(dest => dest.TargetLanguage, opt => opt.MapFrom(src => src.TargetLanguage))
                .ForMember(dest => dest.Domain, opt => opt.MapFrom(src => src.Domain))
                .ReverseMap();

            CreateMap<string, Database.SiteUrl>()
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => src));
        }
    }
}
