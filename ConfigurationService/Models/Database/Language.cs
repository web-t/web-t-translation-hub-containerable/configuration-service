﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tilde.ConfigurationService.Models.Database
{
    public class Language
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(2)]
        public string TargetLanguage { get; set; }

        [MaxLength(255)]
        public string Domain { get; set; }

        [ForeignKey(nameof(Configuration))]
        public Guid ConfigurationId { get; set; }
        public Configuration Configuration { get; set; }
    }
}
