﻿using System.ComponentModel.DataAnnotations;

namespace Tilde.ConfigurationService.Models.Database
{
    public class Provider
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(20)]
        public EMW.Contracts.Enums.Provider.Type Type { get; set; }

        [MaxLength(255)]
        public string? Name { get; set; }

        [MaxLength(255)]
        public string? Url { get; set; }

        [MaxLength(255)]
        public string? Username { get; set; }

        [MaxLength(255)]
        public string? Password { get; set; }

        [MaxLength(255)]
        public string? ApiKey { get; set; }
    }
}
