﻿using System.ComponentModel.DataAnnotations;

namespace Tilde.ConfigurationService.Models.Database
{
	public class Configuration
	{
		[Key]
		public Guid Id { get; set; }

		[MaxLength(2)]
		public string SourceLanguage { get; set; }

		[MaxLength(255)]
		public string Name { get; set; }

		#region relations
		public ICollection<Language> Languages { get; set; }
		public ICollection<SiteUrl> SiteUrls { get; set; }

		#endregion

		#region Timestamps

		public DateTime? CreatedAt { get; set; }

		#endregion
	}
}
