﻿using Microsoft.EntityFrameworkCore;

namespace Tilde.ConfigurationService.Models.Database
{
    public class ServiceDbContext : DbContext
    {
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<SiteUrl> SiteUrls { get; set; }

        public ServiceDbContext(DbContextOptions<ServiceDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
