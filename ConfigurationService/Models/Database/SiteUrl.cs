﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Tilde.ConfigurationService.Models.Database
{
    public class SiteUrl
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(255)]
        public string Url { get; set; }

        [ForeignKey(nameof(Configuration))]
        public Guid ConfigurationId { get; set; }
        public Configuration Configuration { get; set; }
    }
}
