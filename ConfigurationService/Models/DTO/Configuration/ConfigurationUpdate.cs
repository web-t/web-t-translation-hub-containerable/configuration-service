﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Tilde.EMW.Contracts.Models.Validators;
using TildeEMW = Tilde.EMW.Contracts.Models.Services.Configuration.Language;

namespace Tilde.ConfigurationService.Models.DTO.Configuration
{
    public class ConfigurationUpdate
    {
        [Required]
        [MaxLength(255)]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [Required]
        [MaxLength(2)]
        [JsonPropertyName("srcLang")]
        public string SourceLanguage { get; set; }

        [Required]
        [MaxLength(50)]
        [JsonPropertyName("languages")]
        public IEnumerable<TildeEMW.Language> Languages { get; set; }

        [Required]
        [MaxLength(50)]
        [UrlValidator(ErrorMessage = "Provided URL has incorrect format. Please provide correct protocol and domain")]
        [JsonPropertyName("siteUrls")]
        public IEnumerable<string> SiteUrls { get; set; }
    }
}
