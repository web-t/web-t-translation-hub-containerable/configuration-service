﻿using System.Text.Json.Serialization;
using TildeEMW = Tilde.EMW.Contracts.Models.Services.Configuration.Configuration;

namespace Tilde.ConfigurationService.Models.DTO.Configuration
{
    public class ConfigurationList
    {
        [JsonPropertyName("configurations")]
        public IEnumerable<TildeEMW.Configuration> Configurations { get; set; }
    }
}
