﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using TildeEMWEnums = Tilde.EMW.Contracts.Enums;
using TildeEMWConfiguration = Tilde.EMW.Contracts.Models.Services.Configuration;

namespace Tilde.ConfigurationService.Models.DTO.Provider
{
    public class NewProvider
    {
        [MaxLength(255)]
        [JsonPropertyName("name")]
        public string? Name { get; set; }

        [Required]
        [JsonPropertyName("type")]
        public TildeEMWEnums.Provider.Type Type { get; set; }

        [MaxLength(255)]
        [JsonPropertyName("url")]
        public string? Url { get; set; }

        [Required]
        [JsonPropertyName("auth")]
        public TildeEMWConfiguration.Authentication.Authentication Authentication { get; set; }
    }
}
