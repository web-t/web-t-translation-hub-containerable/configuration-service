using Microsoft.AspNetCore.Mvc;
using Tilde.ConfigurationService.Interfaces.Services;
using Tilde.EMW.Contracts.Exceptions.Configuration;
using Tilde.EMW.Contracts.Models.Services.Configuration.Configuration;

namespace Tilde.ConfigurationService.Controllers
{
	[ApiController]
	[Route("configuration")]
	public class ConfigurationController : ControllerBase
	{
		private readonly ILogger<ConfigurationController> _logger;
		private readonly IConfigurationService _configurationService;

		public ConfigurationController(
			ILogger<ConfigurationController> logger,
			IConfigurationService configurationService
		)
		{
			_logger = logger;
			_configurationService = configurationService;
		}

		/// <summary>
		/// List all configurations
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("")]
		public async Task<ActionResult<Models.DTO.Configuration.ConfigurationList>> List()
		{
			var results = await _configurationService.List();
			return Ok(results);
		}

		/// <summary>
		/// Create new configuration
		/// </summary>
		/// <param name="config"> New configuration model </param>
		/// <returns></returns>
		[HttpPost]
		[Route("")]
		public async Task<ActionResult<Configuration>> Create(NewConfiguration config)
		{
			var result = await _configurationService.Create(config);
			return Ok(result);
		}

		/// <summary>
		/// Get configuration
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <returns></returns>
		[HttpGet]
		[Route("{id}")]
		public async Task<ActionResult<Configuration>> Get(Guid id)
		{
			try
			{
				var result = await _configurationService.Get(id);
				return Ok(result);
			}
			catch (ConfigurationNotFoundException ex)
			{
				_logger.LogError(ex, "Configuration not found, please provide existing configuration unique identifier");
				return NotFound();
			}
		}

		/// <summary>
		/// Update configuration
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <param name="config"> Configuration update model </param>
		/// <returns></returns>
		[HttpPut]
		[Route("{id}")]
		public async Task<ActionResult<Configuration>> Update(Guid id, Models.DTO.Configuration.ConfigurationUpdate config)
		{
			try
			{
				var result = await _configurationService.Update(id, config);
				return Ok(result);
			}
			catch (ConfigurationNotFoundException ex)
			{
				_logger.LogError(ex, "Configuration not found, please provide existing configuration unique identifier");
				return NotFound();
			}
		}

		/// <summary>
		/// Delete configuration
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <returns></returns>
		[HttpDelete]
		[Route("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			try
			{
				await _configurationService.Delete(id);
				return Ok();
			}
			catch (ConfigurationNotFoundException ex)
			{
				_logger.LogError(ex, "Configuration not found, please provide existing configuration unique identifier");
				return NotFound();
			}

		}
	}
}