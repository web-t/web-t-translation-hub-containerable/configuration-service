﻿using Microsoft.AspNetCore.Mvc;
using Tilde.ConfigurationService.Interfaces.Services;
using Tilde.ConfigurationService.Models.DTO.Provider;
using Tilde.EMW.Contracts.Models.Services.Configuration.Provider;

namespace Tilde.ConfigurationService.Controllers
{
    [ApiController]
    [Route("provider")]
    public class ProviderController : ControllerBase
    {
        private readonly ILogger<ProviderController> _logger;
        private readonly IProviderService _providerService;

        public ProviderController(
            ILogger<ProviderController> logger,
            IProviderService providerService
        )
        {
            _logger = logger;
            _providerService = providerService;
        }

        /// <summary>
        /// Get provider
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<Provider>> Get()
        {
            var result = await _providerService.Get();
            return Ok(result);
        }

        /// <summary>
        /// Create new provider
        /// </summary>
        /// <param name="newProvider"> New provider model </param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Provider>> Create(NewProvider newProvider)
        {

            var result = await _providerService.Create(newProvider);
            return Ok(result);
        }
    }
}
