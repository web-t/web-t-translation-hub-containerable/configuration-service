﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Tilde.ConfigurationService.Models.Database;

#nullable disable

namespace Tilde.ConfigurationService.Migrations
{
    [DbContext(typeof(ServiceDbContext))]
    [Migration("20221115115442_AddSubjectId")]
    partial class AddSubjectId
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.10")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("Tilde.ConfigurationService.Models.Database.Configuration", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(36)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255)
                        .HasColumnType("varchar(255)");

                    b.Property<string>("SubjectId")
                        .IsRequired()
                        .HasMaxLength(36)
                        .HasColumnType("varchar(36)");

                    b.Property<string>("UserGroup")
                        .IsRequired()
                        .HasMaxLength(255)
                        .HasColumnType("varchar(255)");

                    b.HasKey("Id");

                    b.ToTable("Configurations");
                });

            modelBuilder.Entity("Tilde.ConfigurationService.Models.Database.MTSystemConfiguration", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<Guid>("ConfigurationId")
                        .HasColumnType("char(36)");

                    b.Property<string>("Domain")
                        .IsRequired()
                        .HasMaxLength(255)
                        .HasColumnType("varchar(255)");

                    b.Property<string>("SourceLanguage")
                        .IsRequired()
                        .HasMaxLength(2)
                        .HasColumnType("varchar(2)");

                    b.Property<Guid>("SystemId")
                        .HasColumnType("char(36)");

                    b.Property<string>("TargetLanguage")
                        .IsRequired()
                        .HasMaxLength(2)
                        .HasColumnType("varchar(2)");

                    b.HasKey("Id");

                    b.HasIndex("ConfigurationId");

                    b.ToTable("MTSystemConfigurations");
                });

            modelBuilder.Entity("Tilde.ConfigurationService.Models.Database.MTSystemConfiguration", b =>
                {
                    b.HasOne("Tilde.ConfigurationService.Models.Database.Configuration", "Configuration")
                        .WithMany("MTSystemConfiguration")
                        .HasForeignKey("ConfigurationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Configuration");
                });

            modelBuilder.Entity("Tilde.ConfigurationService.Models.Database.Configuration", b =>
                {
                    b.Navigation("MTSystemConfiguration");
                });
#pragma warning restore 612, 618
        }
    }
}
