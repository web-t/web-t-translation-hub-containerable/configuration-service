﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.ConfigurationService.Migrations
{
    public partial class ChangeStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Configurations_MTSystemConfigurations_MTSystemConfigurationId",
                table: "Configurations");

            migrationBuilder.DropIndex(
                name: "IX_Configurations_MTSystemConfigurationId",
                table: "Configurations");

            migrationBuilder.DropColumn(
                name: "MTSystemConfigurationId",
                table: "Configurations");

            migrationBuilder.RenameColumn(
                name: "Subject",
                table: "Configurations",
                newName: "UserGroup");

            migrationBuilder.AddColumn<Guid>(
                name: "ConfigurationId",
                table: "MTSystemConfigurations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_MTSystemConfigurations_ConfigurationId",
                table: "MTSystemConfigurations",
                column: "ConfigurationId");

            migrationBuilder.AddForeignKey(
                name: "FK_MTSystemConfigurations_Configurations_ConfigurationId",
                table: "MTSystemConfigurations",
                column: "ConfigurationId",
                principalTable: "Configurations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MTSystemConfigurations_Configurations_ConfigurationId",
                table: "MTSystemConfigurations");

            migrationBuilder.DropIndex(
                name: "IX_MTSystemConfigurations_ConfigurationId",
                table: "MTSystemConfigurations");

            migrationBuilder.DropColumn(
                name: "ConfigurationId",
                table: "MTSystemConfigurations");

            migrationBuilder.RenameColumn(
                name: "UserGroup",
                table: "Configurations",
                newName: "Subject");

            migrationBuilder.AddColumn<long>(
                name: "MTSystemConfigurationId",
                table: "Configurations",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Configurations_MTSystemConfigurationId",
                table: "Configurations",
                column: "MTSystemConfigurationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Configurations_MTSystemConfigurations_MTSystemConfigurationId",
                table: "Configurations",
                column: "MTSystemConfigurationId",
                principalTable: "MTSystemConfigurations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
