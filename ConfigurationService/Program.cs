using AutoMapper;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Text.Json.Serialization;
using Tilde.ConfigurationService.Extensions;
using Tilde.ConfigurationService.Interfaces.Services;
using Tilde.ConfigurationService.Models.Configuration;
using Tilde.ConfigurationService.Models.Database;
using Tilde.ConfigurationService.Models.Mappings;
using Tilde.ConfigurationService.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Host
    .UseSerilog((ctx, config) =>
    {
        config
            .Enrich.FromLogContext()
            .WriteTo.Debug()
            .WriteTo.Console()
            .ReadFrom.Configuration(builder.Configuration);
    });

// Add services to the container.

builder.Services.AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });

var configuration = builder.Configuration.GetSection("Configuration").Get<ConfigurationSettings>();

builder.Services.AddDocumentation(configuration);

builder.Services.AddHealthChecks();

builder.Services.Configure<ConfigurationServices>(builder.Configuration.GetSection("Services"));

builder.Services.AddDbContextPool<ServiceDbContext>(options =>
{
    var serviceConfiguration = builder.Configuration.GetSection("Services").Get<ConfigurationServices>()!;
    options.UseMySql(
        serviceConfiguration.Database.ConnectionString,
        ServerVersion.AutoDetect(serviceConfiguration.Database.ConnectionString)
    );
});  
var mappingConfig = new MapperConfiguration(config =>
{
    config.AddProfile(new MappingProfile());
});

builder.Services.AddSingleton(mappingConfig.CreateMapper());

builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IConfigurationService, ConfigurationService>();
builder.Services.AddScoped<IProviderService, ProviderService>();
builder.Services.AddHttpClient();
var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    Log.Information("Migrate database");

    var context = scope.ServiceProvider.GetRequiredService<ServiceDbContext>();
    context.Database.Migrate();

    Log.Information("Migration completed");
}

app.UseDocumentation(configuration);

app.UseRouting();

app.UseAuthorization();

app.MapControllers();

// Startup probe / readyness probe
app.MapHealthChecks("/health/ready", new HealthCheckOptions()
{

});

// Liveness 
app.MapHealthChecks("/health/live", new HealthCheckOptions()
{

});

app.Run();
